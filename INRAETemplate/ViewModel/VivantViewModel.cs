﻿using INRAETemplate.DAO;
using INRAETemplate.Model;
using INRAETemplate.View;
using INRAETemplate.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace INRAETemplate.ViewModel
{
    public class VivantViewModel : BaseViewModel
    {
        #region Attributs
        private int vivantId;
        private string vivantNom;
        //Pour le Picker : attributs vivants et vivantSelectionne
        private ObservableCollection<Vivant> vivants;
        private Vivant vivantSelectionne;
        #endregion

        #region Commandes
        public ICommand EditionCommand { get; private set; }
        #endregion

        #region Propriétés
        public int VivantId
        {
            get { return vivantId; }
            set
            {
                if (vivantId != value)
                {
                    vivantId = value;
                    OnPropertyChanged(nameof(VivantId));
                }
            }
        }

        public string VivantNom
        {
            get { return vivantNom; }
            set
            {
                if (vivantNom != value)
                {
                    vivantNom = value;
                    OnPropertyChanged(nameof(VivantNom));
                }
            }
        }
        //Pour le Picker : propriétés Vivants et VivantSelectionne
        public ObservableCollection<Vivant> Vivants
        {
            get { return vivants; }
            set
            {
                if (vivants != value)
                {
                    vivants = value;
                    OnPropertyChanged(nameof(Vivants));
                }
            }
        }

        public Vivant VivantSelectionne
        {
            get { return vivantSelectionne; }
            set
            {
                if (vivantSelectionne != value)
                {
                    vivantSelectionne = value;
                    OnPropertyChanged(nameof(vivantSelectionne));
                    VivantId = vivantSelectionne.VivantId;
                    VivantNom = vivantSelectionne.VivantNom;
                }
            }
        }
        #endregion

        #region Constructeurs
        public VivantViewModel()
        {
            //Alimentation du Picker à partir de VivantDAO
            Vivants = new ObservableCollection<Vivant>(VivantDAO.GetVivants());
            EditionCommand = new Command(async () => await Edition());
        }
        #endregion

        #region Méthodes
        private async Task Edition()
        {
            IsBusy = true;
            await Task.Delay(1000);
            IDictionary<string, object> proprietes = new Dictionary<string, object>();
            proprietes.Add("IdVivant", VivantId);
            await Shell.Current.GoToAsync(nameof(VivantEditionView), true, proprietes);
            IsBusy = false;
        }
        #endregion

    }
}
