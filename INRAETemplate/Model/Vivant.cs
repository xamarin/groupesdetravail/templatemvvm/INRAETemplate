﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace INRAETemplate.Model
{
    public class Vivant
    {
        public int VivantId { get; set; }
        public string VivantNom { get; set; }

        public Vivant() { }
        public Vivant(int id, string nom)
        {
            this.VivantId = id;
            this.VivantNom = nom;
        }

    }
}
