using INRAETemplate.ViewModel;

namespace INRAETemplate.View;

public partial class VivantEditionView : ContentPage
{
	public VivantEditionView()
	{
		InitializeComponent();
		this.BindingContext = new VivantEditionViewModel();
	}
}