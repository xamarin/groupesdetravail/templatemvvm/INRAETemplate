﻿using INRAETemplate.DAO;
using INRAETemplate.Model;
using INRAETemplate.ViewModel.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace INRAETemplate.ViewModel
{
    [QueryProperty(nameof(VivantId), "IdVivant")]
    public class VivantEditionViewModel : BaseViewModel
    {
        #region Attributs
        private int vivantId;
        private string vivantNom;
        private Vivant vivantDeLaPage;
        #endregion

        #region Commandes
        public ICommand EnregistrerCommand { get; private set; }
        #endregion

        #region Propriétés
        public int VivantId
        {
            get { return vivantId; }
            set
            {
                if (value != 0 && vivantId != value)
                {
                    vivantId = value;
                    OnPropertyChanged(nameof(VivantId));
                    vivantDeLaPage = VivantDAO.GetVivantById(VivantId);
                    VivantNom = vivantDeLaPage.VivantNom;
                }
            }
        }
        public string VivantNom
        {
            get { return vivantNom; }
            set
            {
                if (vivantNom != value)
                {
                    vivantNom = value;
                    OnPropertyChanged(nameof(VivantNom));
                }
            }
        }
        #endregion

        #region Constructeurs
        public VivantEditionViewModel()
        {
            EnregistrerCommand = new Command(async () => await Enregistrer());
        }
        #endregion

        #region Méthodes
        public async Task Enregistrer()
        {
            IsBusy = true;
            await VivantDAO.Save(vivantDeLaPage);
            IsBusy = false;
        }
        #endregion

    }
}
