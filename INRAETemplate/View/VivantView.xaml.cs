using INRAETemplate.ViewModel;

namespace INRAETemplate.View;

public partial class VivantView : ContentPage
{
	public VivantView()
	{
		InitializeComponent();
		this.BindingContext = new VivantViewModel();
	}
}