﻿using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Core;
using INRAETemplate.Model;

namespace INRAETemplate.DAO
{
    public static class VivantDAO
    {
        public static List<Vivant> GetVivants()
        {
            List<Vivant> vivants = new List<Vivant>();
            Vivant v1 = new Vivant(1, "INRAE01");
            Vivant v2 = new Vivant(2, "INRAE02");
            Vivant v3 = new Vivant(3, "INRAE03");
            vivants.Add(v1);
            vivants.Add(v2);
            vivants.Add(v3);
            return vivants;
        }

        public static Vivant GetVivantById(int id)
        {
            Vivant vivant = GetVivants().FirstOrDefault(x => x.VivantId == id);
            return vivant;
        }

        public static async Task Save(Vivant vivant)
        {
            await Task.Delay(1000);
            IToast t = Toast.Make("Enregistrement OK !", ToastDuration.Long);
            await t.Show();
        }
    }
}
