﻿using INRAETemplate.View;

namespace INRAETemplate;

public partial class AppShell : Shell
{
	public AppShell()
	{
		InitializeComponent();
        Routing.RegisterRoute(nameof(VivantView), typeof(VivantView));
        Routing.RegisterRoute(nameof(VivantEditionView), typeof(VivantEditionView));
        Routing.RegisterRoute(nameof(SettingsView), typeof(SettingsView));
        Routing.RegisterRoute(nameof(AboutView), typeof(AboutView));
    }
}
