## .NET MAUI Project and Item Templates

  
#### This repository is to host the .NET MAUI INRAE Project Templates, Item Templates and Code Snippets.
 Release Details:
|Channel|.NET MAUI Version|IDE Version|Release Date|
|:---:|:---:|:---:|:---:|
|Stable|.NET 7 SR3 (7.0.59)|VS2022 17.5.x/17.5.x|Tue, Jan 31, 2023|
|Preview|.NET 8 Preview 2 (8.0.0-preview.2.7871)|VS2022 17.6.0 Preview 2.0|Tue, Mar 14, 2023|


Template with basic project example and has Item Templates for:

* MMVM model
	* Model (c#): 
		* `VivantModel.cs`
	* View (4 views)
		* `VivantView` (main view )
		* `VivantEditionView` (edition mode)
		* `AboutView`
		* `SettingsView`
	* ViewModel  (4 +1)
		* `BaseViewModel` (INotifyPropertyChanged)
		* `VivantViewModel`
		* `VivantEditionViewModel`
	* DAO (Data Access Object)
		* `VivantDAO` vivant db simulation; id, Name (with 3 items)

* Navigation using `shell`
* Main frame with scroll
* Buttons are on the bottom
* Demo of `ActivityIndicator`(VivantView, VivantEditionView)
* Demo of `Picker` (list in VivantView)
* 

## Template vs2022 Windows usage

## Template vs2022 Mac usage

## Renaming the entire solution
 ```shell
 ./rename_solution.sh 1. INRAETemplate MyNewSolution
 ```
